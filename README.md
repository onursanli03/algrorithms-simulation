### **Sorting Algorithm Simulator** ###

**## Simulating ##**

1. Bubble Sort
2. Insertion Sort
3. Selection Sort
4. Quick Sort
5. Merge Sort

### **License** ###

This project and the underlying Onur Şanlı are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)