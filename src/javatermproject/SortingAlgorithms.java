/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatermproject;

import java.awt.Color;
import java.awt.Event;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Random;
import javax.annotation.Generated;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author onurs
 */
public class SortingAlgorithms extends javax.swing.JFrame {

    private LinkedList<record> recordsList = new LinkedList<record>();
    private Timer time;
    JTextField field1, field2;
    //JTextField[] arrField;
    int step = 0;
    int movement = 0, x1, y1, x2, y2, p, move;
    int tmp_data;
    private int[] data = new int[8];
    int _jtField1, _move, _jtField2;
    int count = 0;
    int index = 0;

    /**
     * Creates new form SortingAlgorithms
     */
    public SortingAlgorithms() {
        initComponents();
        this.setLocationRelativeTo(getRootPane());
        inputData();
        configure_jt_area_estimation();

        time = new Timer(p, th_move);
        time.start();

        interval.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                p = interval.getValue();
                if (p == 100) {

                    time.stop();
                } else {
                    time.setDelay(p);
                    time.start();
                }

            }

        });
        setting_interval();

    }

    private void setting_interval() {

        p = 100;
        interval.setValue(p);
        interval.setMinimum(0);
        interval.setMaximum(100);
        interval.setPaintTicks(true);
        interval.setMajorTickSpacing(20);
        interval.setMinorTickSpacing(10);
        interval.setPaintLabels(true);
        Hashtable ht = new Hashtable();
        ht.put(new Integer(0), new JLabel("Fast"));
        ht.put(new Integer(100), new JLabel("Stop"));
        interval.setLabelTable(ht);

    }

    public void configure_jt_area_estimation() {

        if (sortOptions.getSelectedIndex() == 0) {

            txtInformation.setText("Method : " + sortOptions.getSelectedItem()
                    + "\n"
                    + "\n"
                    + "Bubble sort is a simple and well-known sorting algorithm. "
                    + "\n"
                    + "It is used in practice once in a blue moon and "
                    + "\n"
                    + "its main application is to make an introduction to the sorting algorithms."
                    + "\n"
                    + " Bubble sort belongs to O(n^2) sorting algorithms,"
                    + "\n"
                    + " which makes it quite inefficient for sorting large data volumes."
                    + "\n"
                    + " Bubble sort is stable and adaptive."
                    + "Algorithm : "
                    + "\n"
                    + "\n"
                    + "1. Compare each pair of adjacent elements from the beginning of an array and,"
                    + "\n"
                    + " if they are in reversed order, swap them."
                    + "\n"
                    + "2. If at least one swap has been done, repeat step 1. ");

        } else if (sortOptions.getSelectedIndex() == 1) {
            txtInformation.setText("Method : " + sortOptions.getSelectedItem()
                    + "\n"
                    + "\n"
                    + " The insertion sort, although still O(n^2),"
                    + "\n"
                    + " works in a slightly different way."
                    + "\n"
                    + " It always maintains a sorted sublist in the lower positions of the list."
                    + "\n"
                    + " Each new item is then “inserted” back into the previous sublist"
                    + "\n"
                    + " such that the sorted sublist is one item larger. "
                    + "\n"
                    + "\n"
                    + "Algorithm : "
                    + "\n"
                    + " 1 − If it is the first element, it is already sorted. return 1;"
                    + "\n"
                    + " 2 − Pick next element"
                    + "\n"
                    + " 3 − Compare with all elements in the sorted sub-list"
                    + "\n"
                    + " 4 − Shift all the elements in the sorted sub-list that is greater than the"
                    + "\n"
                    + " value to be sorted\n"
                    + " 5 − Insert the value\n"
                    + " 6− Repeat until list is sorted");

        } else if (sortOptions.getSelectedIndex() == 2) {
            txtInformation.setText("Method : " + sortOptions.getSelectedItem()
                    + "\n"
                    + "\n"
                    + " Selection sort is one of the O(n^2) sorting algorithms, "
                    + "\n"
                    + "which makes it quite inefficient for sorting large data volumes. "
                    + "\n"
                    + "Selection sort is notable for its programming simplicity and "
                    + "\n"
                    + "it can over perform other sorts in certain situations "
                    + "Algorithm :"
                    + "\n"
                    + "\n"
                    + "The idea of algorithm is quite simple. "
                    + "\n"
                    + "Array is imaginary divided into two parts - sorted one and unsorted one. "
                    + "\n"
                    + "At the beginning, sorted part is empty,"
                    + "\n"
                    + "while unsorted one contains whole array. "
                    + "\n"
                    + "At every step, algorithm finds minimal element in the unsorted part and "
                    + "\n"
                    + "adds it to the end of the sorted one. "
                    + "\n"
                    + "When unsorted part becomes empty, algorithm stops.");

        } else if (sortOptions.getSelectedIndex() == 3) {
            txtInformation.setText("Method : " + sortOptions.getSelectedItem()
                    + "\n"
                    + "\n"
                    + "Quicksort is a fast sorting algorithm, "
                    + "\n"
                    + "which is used not only for educational purposes, "
                    + "\n"
                    + "but widely applied in practice. "
                    + "\n"
                    + "On the average, it has O(n log n) complexity, making quicksort suitable for "
                    + "\n"
                    + "sorting big data volumes. The idea of the algorithm is quite simple "
                    + "\n"
                    + "and once you realize it, you can write quicksort as fast as bubble sort. "
                    + "\n"
                    + "\n"
                    + "Algorithm :"
                    + "\n"
                    + "The divide-and-conquer strategy is used in quicksort. "
                    + "Below the recursion step is described:"
                    + "\n"
                    + "1. Choose a pivot value. We take the value of the middle element as pivot value,"
                    + "\n"
                    + "but it can be any value, which is in range of sorted values, "
                    + "\n"
                    + "even if it doesn't present in the array."
                    + "\n"
                    + "2. Partition. Rearrange elements in such a way, "
                    + "\n"
                    + "that all elements which are lesser than the pivot go to the left part of the array and "
                    + "\n"
                    + "all elements greater than the pivot, go to the right part of the array. "
                    + "\n"
                    + "Values equal to the pivot can stay in any part of the array. "
                    + "\n"
                    + "Notice, that array may be divided in non-equal parts."
                    + "\n"
                    + "3. Sort both parts. Apply quicksort algorithm recursively to the left and the right parts.");

        } else if (sortOptions.getSelectedIndex() == 4) {
            txtInformation.setText("Method : " + sortOptions.getSelectedItem()
                    + "\n"
                    + "\n"
                    + " Merge Sort follows the rule of  Divide and Conquer. "
                    + "\n"
                    + "But it doesn't divides the list into two halves. "
                    + "\n"
                    + "In merge sort the unsorted list is divided into N sublists, "
                    + "\n"
                    + "each having one element, because a list of one element is considered sorted. "
                    + "\n"
                    + "Then, it repeatedly merge these sublists, to produce new sorted sublists, "
                    + "\n"
                    + "and at lasts one sorted list is produced.\n"
                    + "\n"
                    + "Merge Sort is quite fast, and has a time complexity of O(n log n). "
                    + "\n"
                    + "It is also a stable sort, which means the \"equal\" elements are "
                    + "\n"
                    + "ordered in the same order in the sorted list.");

        }

    }

    private void inputData() {
        Random rnd = new Random();

        if (inputOption.getSelectedItem().equals("Random")) {

            txtInput1.setText(String.valueOf(rnd.nextInt(100)));
            txtInput2.setText(String.valueOf(rnd.nextInt(100)));
            txtInput3.setText(String.valueOf(rnd.nextInt(100)));
            txtInput4.setText(String.valueOf(rnd.nextInt(100)));
            txtInput5.setText(String.valueOf(rnd.nextInt(100)));
            txtInput6.setText(String.valueOf(rnd.nextInt(100)));
            txtInput7.setText(String.valueOf(rnd.nextInt(100)));
            txtInput8.setText(String.valueOf(rnd.nextInt(100)));

            System.out.println("selected");

        } else {

            txtInput1.setText("");
            txtInput2.setText("");
            txtInput3.setText("");
            txtInput4.setText("");
            txtInput5.setText("");
            txtInput6.setText("");
            txtInput7.setText("");
            txtInput8.setText("");

            System.out.println("not selected");

        }
    }

    private void readData() {

        data[0] = Integer.parseInt(txtInput1.getText());
        data[1] = Integer.parseInt(txtInput2.getText());
        data[2] = Integer.parseInt(txtInput3.getText());
        data[3] = Integer.parseInt(txtInput4.getText());
        data[4] = Integer.parseInt(txtInput5.getText());
        data[5] = Integer.parseInt(txtInput6.getText());
        data[6] = Integer.parseInt(txtInput7.getText());
        data[7] = Integer.parseInt(txtInput8.getText());

    }

    private void updateData() {
        txtInput1.setText(String.valueOf(data[0]));
        txtInput2.setText(String.valueOf(data[1]));
        txtInput3.setText(String.valueOf(data[2]));
        txtInput4.setText(String.valueOf(data[3]));
        txtInput5.setText(String.valueOf(data[4]));
        txtInput6.setText(String.valueOf(data[5]));
        txtInput7.setText(String.valueOf(data[6]));
        txtInput8.setText(String.valueOf(data[7]));
    }

    private int randomProcess() {
        int i = (int) (Math.random() * 100.0);
        i = i % 8;
        return i;
    }

    private void orderProcess() {
        if (sortOptions.getSelectedItem().equals("Bubble Sort") && sortOrderOption.getSelectedItem().equals("Ascending")) {
            bubble_asc();
        } else if (sortOptions.getSelectedItem().equals("Bubble Sort") && sortOrderOption.getSelectedItem().equals("Descending")) {
            bubble_dsc();
        } else if (sortOptions.getSelectedItem().equals("Insertion Sort") && sortOrderOption.getSelectedItem().equals("Ascending")) {
            insertion_asc();
        } else if (sortOptions.getSelectedItem().equals("Insertion Sort") && sortOrderOption.getSelectedItem().equals("Descending")) {
            insertion_dsc();
        } else if (sortOptions.getSelectedItem().equals("Selection Sort") && sortOrderOption.getSelectedItem().equals("Ascending")) {
            selection_asc();
        } else if (sortOptions.getSelectedItem().equals("Selection Sort") && sortOrderOption.getSelectedItem().equals("Descending")) {
            selection_dsc();
        } else if (sortOptions.getSelectedItem().equals("Quick Sort") && sortOrderOption.getSelectedItem().equals("Ascending")) {
            quicksort_asc(0, data.length - 1);
        } else if (sortOptions.getSelectedItem().equals("Quick Sort") && sortOrderOption.getSelectedItem().equals("Descending")) {
            quicksort_dsc(0, data.length - 1);
        } else if (sortOptions.getSelectedItem().equals("Merge Sort") && sortOrderOption.getSelectedItem().equals("Ascending")) {
            merge_asc(0, data.length - 1);
        } else if (sortOptions.getSelectedItem().equals("Merge Sort") && sortOrderOption.getSelectedItem().equals("Descending")) {
            merge_dsc(0, data.length - 1);
        }

        count = recordsList.size();
        play();

    }

    private void play() {

        configure_jt_field();
        field1.setBackground(Color.green);
        field2.setBackground(Color.yellow);
        index++;

    }

    private void savingLocation() {
        x1 = field1.getLocation().x;
        x2 = field2.getLocation().x;
        y1 = field1.getLocation().y;
        y2 = field2.getLocation().y;

        movement++;
    }

    private void configure_jt_field() {
        if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 1) {

            field1 = txtInput1;
            field2 = txtInput2;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 2) {

            field1 = txtInput1;
            field2 = txtInput3;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 3) {

            field1 = txtInput1;
            field2 = txtInput4;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 4) {

            field1 = txtInput1;
            field2 = txtInput5;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 5) {

            field1 = txtInput1;
            field2 = txtInput6;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput1;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 0 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput1;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 2) {

            field1 = txtInput2;
            field2 = txtInput3;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 3) {

            field1 = txtInput2;
            field2 = txtInput4;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 4) {

            field1 = txtInput2;
            field2 = txtInput5;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 5) {

            field1 = txtInput2;
            field2 = txtInput6;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput2;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 1 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput2;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 2 && recordsList.get(index).getJtextField2() == 3) {

            field1 = txtInput3;
            field2 = txtInput4;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 2 && recordsList.get(index).getJtextField2() == 4) {

            field1 = txtInput3;
            field2 = txtInput5;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 2 && recordsList.get(index).getJtextField2() == 5) {

            field1 = txtInput3;
            field2 = txtInput6;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 2 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput3;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 2 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput3;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 3 && recordsList.get(index).getJtextField2() == 4) {

            field1 = txtInput4;
            field2 = txtInput5;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 3 && recordsList.get(index).getJtextField2() == 5) {

            field1 = txtInput4;
            field2 = txtInput6;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 3 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput4;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 3 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput4;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 4 && recordsList.get(index).getJtextField2() == 5) {

            field1 = txtInput5;
            field2 = txtInput6;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 4 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput5;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 4 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput5;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 5 && recordsList.get(index).getJtextField2() == 6) {

            field1 = txtInput6;
            field2 = txtInput7;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 5 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput6;
            field2 = txtInput8;
            savingLocation();

        } else if (recordsList.get(index).getJtextField1() == 6 && recordsList.get(index).getJtextField2() == 7) {

            field1 = txtInput7;
            field2 = txtInput8;
            savingLocation();

        }
    }

    public ActionListener th_move = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (movement == 1) {

                field1.setLocation(field1.getLocation().x + 1, field1.getLocation().y);
                field2.setLocation(field2.getLocation().x - 1, field2.getLocation().y);
                step++;

                if (step == 65) {
                    step = 0;
                    movement = 2;
                }

            } else if (movement == 2) {
                if (y1 > y2) {
                    field1.setLocation(field1.getLocation().x, field1.getLocation().y - 1);
                    field2.setLocation(field2.getLocation().x, field2.getLocation().y + 1);
                } else {
                    field1.setLocation(field1.getLocation().x, field1.getLocation().y + 1);
                    field2.setLocation(field2.getLocation().x, field2.getLocation().y - 1);
                }
                step++;

                if (step == get_gap()) {
                    step = 0;
                    movement = 3;
                }

            } else if (movement == 3) {

                field1.setLocation(field1.getLocation().x - 1, field1.getLocation().y);
                field2.setLocation(field2.getLocation().x + 1, field2.getLocation().y);
                step++;

                if (step == 15) {
                    step = 0;
                    movement = 4;
                }
            } else if (movement == 4) {
                behindToOrigin();
                movement = 0;
                configure_jt_area_estimation();
                if (index == recordsList.size()) {
                    index = 0;
                    recordsList.removeAll(recordsList);
                } else {
                    play();
                }
            } else {

            }

        }

        private void behindToOrigin() {
            String temp;
            temp = field1.getText();
            field1.setText(field2.getText());
            field2.setText(temp);
            field1.setBackground(Color.white);
            field2.setBackground(Color.white);
            field1.setLocation(x1, y1);
            field2.setLocation(x2, y2);
        }

        private int get_gap() {
            int gap = 0;
            if (y1 > y2) {
                gap = y1 - y2;
            } else {
                gap = y2 - y1;
            }

            return gap;

        }

    };

    public void bubble_asc() {

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1; j++) {
                if (data[j] > data[j + 1]) {
                    tmp_data = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp_data;
                    _move = 1;
                }

                _jtField1 = j;
                _jtField2 = j + 1;
                if (_move == 1) {
                    recordsList.add(new record(_jtField1, _move, _jtField2));
                }
                _move = 0;

            }
        }

    }

    public void bubble_dsc() {

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1; j++) {
                if (data[j] < data[j + 1]) {
                    tmp_data = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp_data;
                    _move = 1;
                }

                _jtField1 = j;
                _jtField2 = j + 1;
                if (_move == 1) {
                    recordsList.add(new record(_jtField1, _move, _jtField2));
                }
                _move = 0;

            }
        }

    }

    public void insertion_asc() {
        int j, i = 0;

        int sKey;

        for (j = 1; j < 8; j++) {
            sKey = data[j];
            for (i = j - 1; (i >= 0) && (data[i] > sKey); i--) {
                _jtField1 = i;
                _move = 1;
                _jtField2 = i + 1;
                data[i + 1] = data[i];
                recordsList.add(new record(_jtField1, _move, _jtField2));
            }
            data[i + 1] = sKey;
        }

    }

    public void insertion_dsc() {
        int j, i = 0;
        int sKey;

        for (j = 1; j < 8; j++) {
            sKey = data[j];
            for (i = j - 1; (i >= 0) && (data[i] < sKey); i--) {
                _jtField1 = i;
                _move = 1;
                _jtField2 = i + 1;
                data[i + 1] = data[i];
                recordsList.add(new record(_jtField1, _move, _jtField2));
            }
            data[i + 1] = sKey;
        }

    }

    public void merge_asc(int l, int h) {
        int low = l;
        int high = h;
        if (low >= high) {
            return;
        }

        int mid = (low + high) / 2;
        merge_asc(low, mid);
        merge_asc(mid + 1, high);
        int end_low = mid;
        int start_high = mid + 1;

        while ((l <= end_low) && (start_high <= high)) {
            if (data[low] < data[start_high]) {
                low++;
            } else {
                int temp = data[start_high];
                for (int k = start_high - 1; k >= low; k--) {
                    _jtField1 = k;
                    _move = 1;
                    _jtField2 = k + 1;
                    data[k + 1] = data[k];
                    recordsList.add(new record(_jtField1, _move, _jtField2));
                }
                data[low] = temp;
                low++;
                end_low++;
                start_high++;
            }
        }

    }

    public void merge_dsc(int l, int h) {
        int low = l;
        int high = h;
        if (low >= high) {
            return;
        }

        int mid = (low + high) / 2;
        merge_dsc(low, mid);
        merge_dsc(mid + 1, high);
        int endlow = mid;
        int starthigh = mid + 1;

        while ((l <= endlow) && (starthigh <= high)) {
            if (data[low] > data[starthigh]) {
                low++;
            } else {
                int temp = data[starthigh];
                for (int k = starthigh - 1; k >= low; k--) {
                    _jtField1 = k;
                    _move = 1;
                    _jtField2 = k + 1;
                    data[k + 1] = data[k];
                    recordsList.add(new record(_jtField1, _move, _jtField2));
                }
                data[low] = temp;
                low++;
                endlow++;
                starthigh++;
            }
        }

    }

    public void selection_asc() {
        int temp;

        for (int p = 0; p < data.length; p++) {
            temp = data[p];
            _jtField1 = p;
            for (int k = p + 1; k < data.length; k++) {
                if (data[k] < temp) {
                    temp = data[k];
                    _jtField2 = k;
                }
            }

            if (p < data.length - 1) {
                if (data[p] != temp) {
                    data[_jtField2] = data[p];
                    data[p] = temp;
                    recordsList.add(new record(_jtField1, 1, _jtField2));
                }
            }

        }

    }

    public void selection_dsc() {
        int temp;

        for (int p = 0; p < data.length; p++) {
            temp = data[p];
            _jtField1 = p;
            for (int k = p + 1; k < data.length; k++) {
                if (data[k] > temp) {
                    temp = data[k];
                    _jtField2 = k;
                }
            }

            if (p < data.length - 1) {
                if (data[p] != temp) {
                    data[_jtField2] = data[p];
                    data[p] = temp;
                    recordsList.add(new record(_jtField1, 1, _jtField2));
                }
            }

        }

    }

    private void quicksort_asc(int l, int h) {

        int low = l;
        int high = h;

        int mid = data[(low + high) / 2];
        int temp;

        while (low < high) {

            while (data[low] < mid) {

                low++;

            }
            while (data[high] > mid) {
                high--;
            }

            if (low <= high) {
                _jtField1 = low;
                _jtField2 = high;
                temp = data[_jtField1];
                data[_jtField1] = data[_jtField2];
                data[_jtField2] = temp;

                _move = 1;

                low++;
                high--;
                recordsList.add(new record(_jtField1, _move, _jtField2));
            }

        }

        if (l < high) {
            quicksort_asc(l, high);
        }
        if (low < h) {
            quicksort_asc(low, h);
        }

    }

    private void quicksort_dsc(int l, int h) {
        int low = l;
        int high = h;
        int mid = data[(low + high) / 2];
        int temp;

        while (low < high) {

            while (data[low] > mid) {

                low++;

            }
            while (data[high] < mid) {
                high--;
            }

            if (low <= high) {

                _jtField1 = low;
                _jtField2 = high;
                temp = data[_jtField1];
                data[_jtField1] = data[_jtField2];
                data[_jtField2] = temp;

                _move = 1;

                low++;
                high--;
                recordsList.add(new record(_jtField1, _move, _jtField2));
            }

        }

        if (l < high) {
            quicksort_asc(l, high);
        }
        if (low < h) {
            quicksort_asc(low, h);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label2 = new java.awt.Label();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtInput1 = new javax.swing.JTextField();
        txtInput2 = new javax.swing.JTextField();
        txtInput3 = new javax.swing.JTextField();
        txtInput4 = new javax.swing.JTextField();
        txtInput5 = new javax.swing.JTextField();
        txtInput6 = new javax.swing.JTextField();
        txtInput7 = new javax.swing.JTextField();
        txtInput8 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        sortOptions = new javax.swing.JComboBox<>();
        sortOrderOption = new javax.swing.JComboBox<>();
        inputOption = new javax.swing.JComboBox<>();
        btnSort = new javax.swing.JButton();
        interval = new javax.swing.JSlider();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtInformation = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();

        label2.setText("label2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setForeground(new java.awt.Color(0, 255, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 204));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Animation");

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Options Algortihm's");

        sortOptions.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bubble Sort", "Insertion Sort", "Selection Sort", "Quick Sort", "Merge Sort" }));
        sortOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sortOptionsİtemStateChanged(evt);
            }
        });

        sortOrderOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ascending", "Descending" }));

        inputOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Random", "Manual" }));
        inputOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                inputOptionİtemStateChanged(evt);
            }
        });

        btnSort.setText("Sort");
        btnSort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSortActionPerformed(evt);
            }
        });

        interval.setMinorTickSpacing(5);
        interval.setPaintLabels(true);
        interval.setPaintTicks(true);
        interval.setSnapToTicks(true);

        jLabel3.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 0, 153));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Algorithm's Information");

        jScrollPane1.setEnabled(false);

        txtInformation.setEditable(false);
        txtInformation.setBackground(new java.awt.Color(0, 204, 204));
        txtInformation.setColumns(20);
        txtInformation.setRows(5);
        jScrollPane1.setViewportView(txtInformation);

        jLabel4.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 51));
        jLabel4.setText("© 2017 Onur Şanlı all rights reserved");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(157, 157, 157)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(sortOrderOption, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(txtInput1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(txtInput2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(416, 416, 416)
                        .addComponent(interval, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(779, 779, 779)
                        .addComponent(btnSort, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(660, 660, 660)
                        .addComponent(inputOption, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(428, 428, 428)
                        .addComponent(sortOptions, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 56, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtInput4, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtInput3, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txtInput5, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtInput6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtInput7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtInput8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(136, 136, 136)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(interval, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(sortOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(btnSort))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(inputOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(130, 130, 130)
                                .addComponent(txtInput2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(170, 170, 170)
                                .addComponent(txtInput3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(txtInput4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtInput5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtInput6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtInput7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtInput8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(15, 15, 15)
                                .addComponent(sortOrderOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(11, 11, 11)
                        .addComponent(txtInput1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(85, 85, 85)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSortActionPerformed

        readData();
        recordsList.clear();
        orderProcess();
    }//GEN-LAST:event_btnSortActionPerformed

    private void sortOptionsİtemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sortOptionsİtemStateChanged

        configure_jt_area_estimation();

    }//GEN-LAST:event_sortOptionsİtemStateChanged

    private void inputOptionİtemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_inputOptionİtemStateChanged

        setting_interval();
        inputData();


    }//GEN-LAST:event_inputOptionİtemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SortingAlgorithms.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SortingAlgorithms.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SortingAlgorithms.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SortingAlgorithms.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SortingAlgorithms().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSort;
    private javax.swing.JComboBox<String> inputOption;
    private javax.swing.JSlider interval;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label2;
    private javax.swing.JComboBox<String> sortOptions;
    private javax.swing.JComboBox<String> sortOrderOption;
    private javax.swing.JTextArea txtInformation;
    private javax.swing.JTextField txtInput1;
    private javax.swing.JTextField txtInput2;
    private javax.swing.JTextField txtInput3;
    private javax.swing.JTextField txtInput4;
    private javax.swing.JTextField txtInput5;
    private javax.swing.JTextField txtInput6;
    private javax.swing.JTextField txtInput7;
    private javax.swing.JTextField txtInput8;
    // End of variables declaration//GEN-END:variables

}
